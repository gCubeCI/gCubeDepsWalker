# gCube Dependency Walker Pipeline
Given a Jenkins project P:
 - navigate the maven modules built by P
 - recursively analyze the module's dependencies and their jenkins projects
 - generate the list of projects that have modules depending on each maven module in P
 - print a formatted report of the information extracted from the analysis.
 
## Requirements
* [Jenkins](https://jenkins.io/) ver. 2.164.2 or newer
* [Pipeline plugin](https://wiki.jenkins.io/display/JENKINS/Pipeline+Plugin)
* [Pipeline: Maven](https://plugins.jenkins.io/pipeline-maven)
* [Pipeline: Basic Steps](https://plugins.jenkins.io/workflow-basic-steps) 
* [Pipeline: Dependency Walker](https://plugins.jenkins.io/pipeline-dependency-walker/) 

## Documentation
* [jenkins.model.Jenkins](https://javadoc.jenkins-ci.org/jenkins/model/Jenkins.html)
* [jenkins.model.ItemGroup](https://javadoc.jenkins-ci.org/hudson/model/ItemGroup.html)